Angler
======

This is my firts project with Objective-C and Cocoa on OSX.

It was mentioned to win a stupid TV game. The busty girl was waving the money,
requesting people guess, how many 4-angles are on the picture.

This app is a tool which allows you to draw the shape and found all the
n-angles.

Unfortunately, the TV show was terminated before I finished the app.


The messy code
==============

I have to sorry for the code. I learned Cocoa and Obj-C on it. So, the design
and code quality was secondary.

I made an attempt to wrap some Obj-C constructions (which I don't like) by my
favourite C++ generics (which I like). It has shown it wasn't a good idea.
Messy...

However, the application itself is 100% functional with no know bugs. It's UI
is impressive. :-)

====

You can see a result example at:
http://smrt28.cz/9.pdf

